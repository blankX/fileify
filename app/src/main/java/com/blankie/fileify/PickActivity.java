package com.blankie.fileify;

import android.os.Bundle;
import android.app.Activity;
import android.widget.Toast;
import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.util.Log;

import com.blankie.fileify.R;

public class PickActivity extends Activity {
    public static final String TAG = "fileify";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        Intent newIntent = new Intent(Intent.ACTION_GET_CONTENT);

        newIntent.addCategory(Intent.CATEGORY_OPENABLE)
            // from Telegram X, why not?
            .putExtra("android.content.extra.SHOW_ADVANCED", true);
        if (intent.hasExtra(Intent.EXTRA_ALLOW_MULTIPLE)) {
            newIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,
                intent.getBooleanExtra(Intent.EXTRA_ALLOW_MULTIPLE, false));
        }
        if (intent.getType() == "vnd.android.cursor.dir/image") {
            newIntent.setType("image/*");
        } else if (intent.getType() == "vnd.android.cursor.dir/video") {
            newIntent.setType("video/*");
        } else {
            newIntent.setType(intent.getType());
        }

        try {
            startActivityForResult(newIntent, 0);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, getString(R.string.no_activity));
            Toast.makeText(getApplicationContext(), R.string.no_activity,
                Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
        Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            Log.d(TAG, "Received resultCode = " + resultCode);
            setResult(RESULT_CANCELED);
            finish();
            // probably not necessary but just in case or something
            return;
        } else if (data == null) {
            Log.d(TAG, "Received data = null");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (data.getData() != null) {
            intent.setData(data.getData());
            setResult(RESULT_OK, intent);
        } else if (data.getClipData() != null) {
            // unsafe?
            intent.setClipData(data.getClipData());
            setResult(RESULT_OK, intent);
        } else {
            Log.d(TAG, "Received data.getData() = null and data.getClipData() = null");
            setResult(RESULT_CANCELED);
        }
        finish();
    }
}
